module.exports = {
  'ContactUs':'تماس با ما',
  'AboutUs':'درباره ما',
  'Blog':'بلاگ',
  'dir':'rtl',
  'dirs':'rtl',
  'Entry Date':'تاریخ ورود',
  'Visa Type':'نوع ویزا',
  'Return Date':'تاریخ بازگشت',
  'Number of passengers':'تعداد مسافران',
  'Number of Kids (Under five years)':'تعداد کودکان (زیر پنج سال)',

  'Services':'خدمات ها',
  'send':'ارسال',
  'Home':'خانه',
  'Health Tourism':'گردشگری سلامت',
  'International Beauty and Health Services':'خدمات بین المللی زیبایی و درمانی',
  'Hospitality And Garantie':'هتل ها و گارانتی',
  'Insurance':'بیمه',
  'tourpackage':'پکیج های تور',
  'readmore':'بیشتر بخوانید',
  'Medical Tourism In Iran':' (گردشگری سلامت در ایران)',
  'RequestForm':' فرم درخوست',
  'country':'کشور',

  'tourismangels24':'فرشتگان توریست ',
  'Request':'درخواست',
  'Tour Package':'پکیچ تورها',
  'pricelist':'لیست قیمت',
  'Visa & Ticket':'ویزا و بلیط',
  'Hospitality':'هتل داری',
  'birthday':'تولد',
  'Mac remedy(digital hospital)':'مک رمدی (بیمارستان آنلاین)',
  'Registering in macremedy24 digital online hospital':'ثبت نام در بیمارستان آنلاین دیجیتال macremedy24',
  'Medical services at the hotel about your recent illnesses in iran':'خدمات پزشکی در هتل  های در ایران',
  'description':'توضیحات',
  'Internet consulting / telemedicine(online multimedia medical consulting)':'مشاوره اینترنتی / پزشکی از راه دور (مشاوره پزشکی چندرسانه ای آنلاین)',
  'Visa':'ویزا',
  'Ticket':'بلیط',
  'Translator':'مترجم',
  'OneSideTicket':'بلیط یک طرفه',
  'Price':'قیمت',
  'TwoSideTicket':'بلیط دو طرفه',
  'FreeServices':'خدمات رایگان',
  'Other Services':'خدمات دیگر',
  'Beauty surgery And others':'جراحی زیبایی و دیگران',
  'name':'نام',
  'Number of adults':'تعداد بزرگسالان',
  'Number of kids':'تعداد کودکان',
  'Countryoforigin':'کشور اصلی',
  'Package':'پکیج',

  'electronimedicalcases':'پرونده پزشکی',
  'Form':'فرم',
  'Terms and Conditions':'قوانین و مقررات',
  'Complaints and follow-up':'شکایات و پیگیری',
  'MacRemedy Portal':'پرتال مک رمدی',
  'Nursing Care':'مراقبت های پرستاری',
  'Nursing and medical clinical care':'مراقبت های بالینی پرستاری و پزشکی',
  'Department_of_specialized clinics':'دپارتمان کلینیک های _ تخصصی',
  'Medical Equipment':'تجهیزات پزشکی',
  'Traditional medicine clinic':'کلینیک طب سنتی',
  'Psychology Clinic':'کلینیک روانشناسی',
  'Womens Clinic':'کلینیک زنان',
  'Family Clinic':'کلینیک خانواده',
  'Ostomic bed sores heal':'زخم بستر استومیک بهبود می یابد',
  'Imaging':'تصویربرداری',
  'Laboratory':'آزمایشگاه',
  'Clinic Rehabilitation':'توانبخشی کلینیک',
  'beauty clinic':'کلینیک زیبایی',
  'Other':'دیگر',
  'Therapeutic':'درمانی',
  'Beauty service':'خدمات زیبایی',
  'Checkup':'معاینه',

  'Service':'سرویس',
  'comment':'توضیح',
  'Send Request':'فرستادن درخواست',
  'target':'هدف',

  'Name':'نام',
  'LastName':'نام خانوادگی',
  'address':'آدرس',
  'Email':'ایمیل',
  'text':'نوشته',
  'mail':'ایمیل',
  'email':'ایمیل',
  'supportphone':'شماره پشتیبانی',
  'OURMISSION':'خط مشی',
  'phone':'با ما در تماس باشید',
  'STAYCONNECTEDWITHUS':'با ما در تماس باشید',
  'phoneorwhatsapp':'واتس آپ یا تلفن',
  'selectcountry':'انتخاب کشور',
  'Subject':'موضوع',
  'Description':'توضیحات',
  'sendrequest':'ارسال درخواست',
  'send':'ارسال ',
  'visa':'ویزا',
  'about':'درباره',
  'Us':'ما',
  'RequestYourQuest':'درخواست خود را ارسال کنید',
  'onphoneconsultancy':'مشاوره آنلاین',
  'whatsapp':'whatsapp',
  'Tours':'تورها',
  'InternationalMedicalTouristGroup':'گروه گردشگری پزشکی بین المللی',
}
