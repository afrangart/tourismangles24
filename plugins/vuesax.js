import Vue from 'vue'
import Vuesax from 'vuesax'

import 'boxicons/css/boxicons.min.css'
import 'vuesax/dist/vuesax.css' //Vuesax styles

Vue.use(Vuesax)
Vue.prototype.$url = 'https://api.tourismangels24.com/index.php/api/';
Vue.prototype.$storage ='https://api.tourismangels24.com';
export default ({app}) => {
  app.$url = 'https://api.tourismangels24.com/index.php/api/'
}
import vSelect from 'vue-select'

Vue.component('v-select', vSelect)

import NuxtSSRScreenSize from 'nuxt-ssr-screen-size'
Vue.use(NuxtSSRScreenSize)

