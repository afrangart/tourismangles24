const axios = require('axios');
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'TourismAngels',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },

    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/icon/logo.ico' },

      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "55x57", href: `/icon/apple-icon-57x57.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "60x60", href: `/icon/apple-icon-60x60.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "72x72", href: `/icon/apple-icon-72x72.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "76x76", href: `/icon/apple-icon-76x76.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "114x114", href: `/icon/apple-icon-114x114.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "120x120", href: `/icon/apple-icon-120x120.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "144x144", href: `/icon/apple-icon-144x144.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "152x152", href: `/icon/apple-icon-152x152.png`},
      {rel: 'apple-touch-icon', type: 'image/x-icon', sizes: "180x180", href: `/icon/apple-icon-180x180.png`},

      {rel: 'icon', type: 'image/png', sizes: "192x192", href: `/icon/android-icon-192x192.png`},
      {rel: 'icon', type: 'image/png', sizes: "32x32", href: `/icon/favicon-32x32.png`},
      {rel: 'icon', type: 'image/png', sizes: "96x96", href: `/icon/favicon-96x96.png`},
      {rel: 'icon', type: 'image/png', sizes: "16x16", href: `/icon/favicon-16x16.png`},
  
    ],

    script: [
      { src: 'google-site-verification=0ACzp9hUBFumOsRl6_6Y7mWz5Vx1wWFFP9nF_pMxvPQ',async:'true'  },

      { innerHTML: 'window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}; gtag(\'js\', new Date());gtag(\'config\', \'G-VXE1F53V4S\');', type: 'text/javascript', charset: 'utf-8'}
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'vuesax/dist/vuesax.css',
   'vue-select/dist/vue-select.css',

    'vuetify/dist/vuetify.min.css'
],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [


    '~/plugins/vuesax.js',
    { src: '~plugins/ssr.js', ssr: false },

  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/vuetify',
    '@nuxtjs/sitemap'

  ],

  sitemap: {

    routes: async () => {
      const { data } = await axios.get('https://api.tourismangels24.com/api/firstpage/create')
      return data
    }


  },
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    ['nuxt-i18n', {
      locales: [
        {
          code: 'en',
	  domain :'tourismangels24.com',
          file: 'en.js'
        },
        {
          code: 'fa',
	  domain :'fa.tourismangels24.com',

          file: 'fa.js'
        },
        {
          code: 'tr',
	  domain :'tr.tourismangels24.com',

          file: 'tr.js'
        },

        {
          code: 'ar',
	  domain :'ar.tourismangels24.com',

          file: 'ar.js'
        }
      ],
      lazy: true,
      langDir: 'lang/',
       differentDomains: true,
      defaultLocale: 'en'
    }]

  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
  server: {
    port: 3265, // default: 3000
   host: '0.0.0.0'
  },
}
